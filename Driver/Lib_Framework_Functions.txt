'*****************************************************************************************************************************************************
'	Name: Lib_Framework_Functions
'	Purpose:This Lib_Framework_Functions related to Libarry framework functions
'	Creator: Kisreddy
'   Param: 	GBL_FrameworkPath			 			| required AllowedRange:  		'Description: Related to Frameworkpath
'   Param: 	GBL_Librariespath			 			| required AllowedRange:  		'Description: Related to GBL_Librariespath 
'   Param: 	GBL_Repositoriespath			 			| required AllowedRange:  		'Description: Related to GBL_Repositoriespath
'   Param: 	GBL_Testspath			 			| required AllowedRange:  		'Description: Related to GBL_Testspath 
'   Param: 	GBL_Resultspath			 			| required AllowedRange:  		'Description: Related to GBL_Resultspath 
'   Param: 	GBL_TestDatapath			 			| required AllowedRange:  		'Description: Related to FilePath 
'   Param: 	GBL_Temppath			 			| required AllowedRange:  		'Description: Related to GBL_TestDatapath 
'   Pre-Requisites: Master data excel should present in path specifiied in script & report should prsent in the application format 

'********************************************************************** *********************************************************************************


'On error resume next

GBL_FrameworkPath=split(environment("TestDir"),"Tests")(0)

GBL_Librariespath=GBL_FrameworkPath&"Libraries\"

GBL_Repositoriespath=GBL_FrameworkPath&"Repositories\"

GBL_Testspath=GBL_FrameworkPath&"Tests\"

GBL_Resultspath=GBL_FrameworkPath&"Results\"

GBL_TestDatapath=GBL_FrameworkPath&"TestData\"

GBL_Temppath=GBL_FrameworkPath&"Temp\"

Call fn_LoadRepositories

call fn_LoadLibraries

Call fn_LoadTestdata
'
'Public Function fn_LoadLibraries
'
'datatable.import
'End function
'




Public Function fn_FileExists(FilePath)

  Set fso = CreateObject("Scripting.FileSystemObject")
  If fso.FileExists(FilePath) Then
    fn_FileExists=True
  Else
    fn_FileExists=False
  End If
 set fso=Nothing
 
End Function

'*****************************************************************************************************************************************************
'	Name: fn_LoadLibraries
'	Purpose:This funtion related to open funtion labraries
'	Creator: Kisreddy
'   Param: 				 			| required AllowedRange:  		'Description:  
'   Pre-Requisites: Master data excel should present in path specifiied in script & report should prsent in the application format 

'********************************************************************** *********************************************************************************


Public Function fn_LoadLibraries

	Set objFSO1 = CreateObject("Scripting.FileSystemObject")
	Set objFolder1 = objFSO1.GetFolder(GBL_Librariespath)
	Set colFiles1 = objFolder1.Files

	For Each objFile1 in colFiles1

		If instr(objFile1.Name,".lck")<=0 Then
			LoadFunctionLibrary GBL_Librariespath&objFile1.Name
		End If
		
	Next
	
	set objFSO1=Nothing
	Set objFolder1=Nothing
	
End Function


'*****************************************************************************************************************************************************
'	Name: fn_LoadRepositories
'	Purpose:This funtion related to LoadRepositories
'	Creator: Kisreddy
'   Param: 				 			| required AllowedRange:  		'Description:  
'   Pre-Requisites: Master data excel should present in path specifiied in script & report should prsent in the application format 

'********************************************************************** *********************************************************************************

Public Function fn_LoadRepositories

	repositoriesCollection.RemoveAll

	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objFolder = objFSO.GetFolder(GBL_Repositoriespath)
	Set colFiles = objFolder.Files

	For Each objFile in colFiles
		If instr(objFile.Name,".lck")<=0 Then
			RepositoriesCollection.Add GBL_Repositoriespath&objFile.Name
		End If
	Next

	set objFSO=Nothing
	Set objFolder=Nothing
	
End Function

'*****************************************************************************************************************************************************
'	Name: fn_LoadTestdata
'	Purpose:This funtion related to Load test data
'	Creator: Kisreddy
'   Param: 				 			| required AllowedRange:  		'Description:  
'   Pre-Requisites: Master data excel should present in path specifiied in script & report should prsent in the application format 

'********************************************************************** *********************************************************************************


Public Function fn_LoadTestdata

   if fn_FileExists(GBL_TestDatapath&Environment("TestName")&".xlsx") Then
      datatable.Import GBL_TestDatapath&Environment("TestName")&".xlsx"
      reporter.ReportEvent micPass,"Test data loading","Loaded successfully"
   else
 	  reporter.ReportEvent micWarning,"Test data loading","Test data file missing"
   End If
   
End Function

