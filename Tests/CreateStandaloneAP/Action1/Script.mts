﻿ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"

Dim APName,Duration,Competence,Machine,Sequence,Materials,Engineers,URLEdit,StandByCat

APName = "AP"&Now()
APName = Replace(APName,"/","")
APName = Replace(APName,":","")

Duration = fn_getdatatablevalue("Duration")
Competence = fn_getdatatablevalue("Competence")
Machine = fn_getdatatablevalue("Machine")
Sequence = fn_getdatatablevalue("Sequence")
Materials = fn_getdatatablevalue("Materials")
Engineers = fn_getdatatablevalue("Engineers")
URLEdit = fn_getdatatablevalue("URLEdit")
StandByCat = fn_getdatatablevalue("StandByCat")

Call fn_FP_Login()
Call fn_CreateStandaloneActionPlan(APName,Duration,Competence,Machine,Sequence,Materials,Engineers,URLEdit,StandByCat)
Call fn_VerifytheAPinActionBacklog(APName)
		
SystemUtil.CloseProcessByName "MSedge.exe"


