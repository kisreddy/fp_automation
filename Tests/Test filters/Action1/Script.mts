﻿ExecuteFile split(environment("TestDir"),"Tests")(0)&"\Driver\Lib_Framework_Functions.txt"

Dim APName,Duration,Competence,Machine,Sequence,Materials,Engineers,URLEdit,StandByCat,SeqFilter

APName = "AP"&Now()
APName = Replace(APName,"/","")
APName = Replace(APName,":","")

Duration = fn_getdatatablevalue("Duration")
Competence = fn_getdatatablevalue("Competence")
Machine = fn_getdatatablevalue("Machine")
Sequence = fn_getdatatablevalue("Sequence")
Materials = fn_getdatatablevalue("Materials")
Engineers = fn_getdatatablevalue("Engineers")
URLEdit = fn_getdatatablevalue("URLEdit")
StandByCat = fn_getdatatablevalue("StandByCat")
SeqFilter = "Sequence: "&Sequence

Call fn_FP_Login()
Call fn_CreateStandaloneActionPlan(APName,Duration,Competence,Machine,Sequence,Materials,Engineers,URLEdit,StandByCat)
Call fn_VerifyTestFilters(Machine,Competence,SeqFilter,APName)	
		
SystemUtil.CloseProcessByName "MSedge.exe"

'Public Function fn_VerifyTestFilters(Machine,Competence,SeqFilter,APName)
'	
'	Browser("Login").Page("Fleet Planner - Fleet").WebList("Machine(s)").Select Machine
'	Browser("Login").Page("Fleet Planner - Fleet").WebList("Competence(s)").Select Competence
'	Browser("Login").Page("Fleet Planner - Fleet").WebList("Status").Select SeqFilter
'	Browser("Login").Page("Fleet Planner - Fleet").WebList("Standby Category").Select StandByCat
'	'Browser("Login").Page("Fleet Planner - Fleet").WebList("Planned start/standby").Select
'	Browser("Login").Page("Fleet Planner - Fleet").WebEdit("Search anything").Set APName
'	Browser("Fleet Planner - Fleet").Page("Fleet Planner - Fleet").WebButton("ApplytheFilters").Click
'	
'	Set ObjDesc1 = Description.Create
'	ObjDesc1("micclass").Value = "WebButton"
'	ObjDesc1("innertext").value = ".*"&APName&".*"
'	
'	If Browser("Login").Page("Fleet Planner - Fleet").WebElement(ObjDesc1).Exist(5) Then	
'		Reporter.ReportEvent micPass, "Verify the Filtered data","Filters are working and filtered data is displayed"
'		Browser("Login").Page("Fleet Planner - Fleet").WebElement(ObjDesc1).Highlight
'		Call CaptureScreen	
'	Else
'		Reporter.ReportEvent micFail, "Verify the Filtered data","Filters are working and filtered data is displayed"
'		Call CaptureScreen
'	End If
'	
'End Function


'Browser("Login").Page("Fleet Planner - Fleet").WebList("Machine(s)").Click	
'	Browser("Login").Page("Fleet Planner - Fleet").WebList("Machine(s)").CustomSet(Machine)

	
	Browser("Login").Page("Fleet Planner - Fleet").WebList("Status").Click
	Browser("Login").Page("Fleet Planner - Fleet").WebList("Status").CustomSet(SeqFilter)
